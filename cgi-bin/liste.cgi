#!/usr/bin/env python3

import cgi, os

numero = 0

params ={}
for clef in cgi.FieldStorage().keys():
  params[clef] = cgi.FieldStorage()[clef].value

print ("Content-type: text/html\n\n",
       "<html>",
       "<head>",
       "<meta charset='UTF-8'>",
       "<style>",
       "* {font-size: 9pt; font-weight:bold;}",
       "</style>",
       "<script type='text/javascript' src='http://localhost/~namari/JS/ouvertureFermeture.js'></script>",
       "</head>",
       "<body>")

def arborescence(repertoire):
    global numero
    liste = os.listdir(repertoire)
    for fichier in sorted(liste):
        if os.path.isdir(repertoire+"/"+fichier) and os.access(repertoire+"/"+fichier, os.X_OK) :
           
            print("<div name='maDiv'>",
            "<img onClick = ouvertureFermeture(this,"+str(numero)+") width = '70px' height = '70px' src = 'http://localhost/~namari/images/folder-close-icon.png' />",           
            "<br/>",
            "<a target='main' href='http://localhost/~namari/cgi-bin/contenu.cgi?repertoire="+repertoire+"/"+fichier+"'>"+fichier,
            "</a>",
            "</div>",
            "<div name='taDiv' id="+str(numero)+" style='margin-left:25%; display:none;'>")
           
            numero += 1
            arborescence(repertoire+"/"+fichier)
            print("</div>")
         

arborescence(params['repertoire'])
print("</body>", "</html>")
