#!/usr/bin/env python3
#-*- coding: utf-8 -*-
import re, cgi
entrees = []
plats = []
desserts = []
fd = open("plats.txt", "r")
for ligne in fd.readlines() :
	res = re.search("(.*) : (.*) \((.*)\)", ligne)
	if res :
		if res.group(1) == "entr ́ee" : entrees.append([res.group(2), res.group(3)])
		if res.group(1) == "plat" : plats.append([res.group(2), res.group(3)])
		if res.group(1) == "dessert" : desserts.append([res.group(2), res.group(3)])

print("""Content-type: text/html\n\n
<html>
	<body>
		<h2> Liste des plats </h2>
		<form action="localhost/~login/calculDuPrix.cgi">""")

print("<h3> Entr ́ees </h3>")
for entree in entrees :
	print(entree[0], ’<input name="’+entree[0]+’" value="’+entree[1]+’" /><br/>’)

print("<h3> Plats </h3>")
for plat in plats :
	print(plat[0], ’<input name="’+plat[0]+’" value="’+plat[1]+’" /><br/>’)

print("<h3> Desserts </h3>")
for dessert in desserts :
	print(dessert[0], ’<input name="’+dessert[0]+’" value="’+dessert[1]+’" /><br/>’)

print("</form></body></html>")
