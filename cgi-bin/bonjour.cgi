#!/usr/bin/env python
# _*_ coding:utf-8 _*_
import cgi
params = {}
for p in cgi.FieldStorage() :
	params[p] = cgi.FieldStorage()[p].value

def calcul(i) :
	r = 0;
	for p in params :
		if int(p) <= int(i) :
			r += int(params[p])
	return r

print("""Content-type: text/html
<html>
<head>
	<meta charset="utf8" />
</head>
<body>
	<center>
	<h3> Examen Syst`emes </h3>""")
print("""<form action="http://localhost/cgi-bin/examen_HMIN113M_2015_2016.cgi">
""")

for p in params :
	r = calcul(p)
	print("<input name=’"+p+"’ type=’checkbox’ value=’"+str(r)+"’/>"+p+" : "+params[p]+"<br/>")

print("""
<br/> <input type="submit" value="Envoi" />
</form>
	</center>
	</body>
</html>""")